#!/usr/bin/make -f
include /usr/share/dpkg/default.mk
export PYBUILD_DESTDIR = debian/cdist

%:
	dh $@ --with python3,linktree,sphinxdoc,bash_completion \
	    --buildsystem=pybuild

cdist/version.py:
	echo 'VERSION = "$(DEB_VERSION_UPSTREAM)"' > cdist/version.py

override_dh_auto_configure: cdist/version.py
	dh_auto_configure

override_dh_auto_build: cdist/version.py
	# This directory is lost during
	#
	# upstream git -> tarball -> git-buildpackage
	#
	# process, since .gitignore files are excluded by
	# git-archive(1) and tarballs can store empty directories,
	# but git can not.
	#
	# Without this directory, build from git repository fails.
	mkdir -p docs/src/man7
	$(MAKE) man html

# This file contains instruction how to install cdist system-wide. It
# gets installed in wrong place and actually redundant.
override_dh_auto_install:
	dh_auto_install
	find debian/cdist/usr/lib -name 'README' -delete

override_dh_install:
	dh_install
	jdupes --link-soft --recurse debian/cdist-doc

# If something has shebang, then `chmod +x'. But it is important to
# search only among scripts, since some manpages trigger false-positive
# (compressed manpages are considered to have single line by head(1),
# and somewhere in that line #! sometimes occurs)
override_dh_fixperms:
	dh_fixperms
	find debian/cdist/usr/lib/python3 -type f -execdir \
	    /bin/sh -c '(head -n1 {} | grep -qF "#!") && chmod +x {}' \;

override_dh_auto_clean: cdist/version.py
	$(MAKE) clean
	dh_auto_clean
	rm -rf ./build cdist/version.py
